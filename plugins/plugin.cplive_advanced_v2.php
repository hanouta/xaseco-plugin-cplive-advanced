<?php

/**
 * Checkpoints Live plugin by Lambda
 * Completeley modified and advanced by b4card1
 * 18-07-2011, tested on PHP v5.3.5 Windows
 **/

class CPLiveAdvanced {
    
    // Config
    public $MAX_DISPLAY_ROWS = 12;
    
    public $POS_X = -64.4;
    public $POS_Y = 22.7;
    
    // In seconds
    public $INTERVAL_NICK_CHANGE = 0;
    
    public $BACKGROUND = false;
    public $INDICES = false;
    
    public $WHITE_NICKS_ALLOWED = true;
    public $WHITE_NICK = false;
    
    // Keys: 0 = None | 1 = F5 | 2 = F6 | 3 = F7
    public $TOGGLE_KEY = 1;
    
    // Don't modify!
    public $ID_TITLE_BAR = 1928378;
    public $ID_LIST = 1928379;
    public $ID_TOGGLE_KEY = 1928380;
    
    public $ANSWER_TOGGLE_HUD = '01928390';
    public $ANSWER_SWITCH_COLOR = '01928396';
    
    public $KEYS = array("No Key", "F5", "F6", "F7");
    
    public $COLORS = array("Title" => "FFE", "TrackText" => "ABC", 
        "TrackNumber" => "9B1", "Position" => "567", "CPText" => "F93", 
        "CPNumber" => "F93", "Time" => "FFC");
    
    // Fields
    private $players;
    private $list;
    
    private $numberCPs;
    private $lastUpdate;
    private $needsUpdate;
    
    public function __construct() {
        $this->players = array();
        $this->needsUpdate = false;
    }

    public function onSync($aseco) {
        $this->getTrackInfo($aseco);
        $this->lastUpdate = $this->getMilliSeconds();
    }
    
    private function bindToggleKey($aseco, $command) {
        $login = $command->login;
        $xml = '<manialink id="' . $this->ID_TOGGLE_KEY . '">';
        $xml .= '<quad action="' . $this->ANSWER_TOGGLE_HUD . '" actionkey="' . $this->TOGGLE_KEY . '" sizen="0 0"  posn="70 70 1"/>';
        $xml .= '</manialink>';
        $aseco->client->addCall("SendDisplayManialinkPageToLogin", array(
            $login, $xml, 0, false));
    }
    
    public function manageChatCommands($aseco, $command) {
        $player = $command['author'];
        $login = $player->login;
        $args = explode(' ', $command['params'], 1);
        $command = !empty($args) ? $args[0] : "";
        
        switch ($command) {
            case "color":
                $this->changeNickDisplay($aseco, $login);
                break;
            case "toggle":
                $maniaLink = $this->players[$login]["Manialink"];
                $this->players[$login]["Manialink"] = !$maniaLink;
                $this->showListToLogin($aseco, $login);
                break;
            default:
                $message = sprintf(
                    "\$gCurrently supported CPLive commands:\n color, toggle(%s)",
                    $this->KEYS[$this->TOGGLE_KEY]
                );
                $aseco->client->query('ChatSendToLogin', $aseco->formatColors($message), $player->login);
        }
    }
    
    private function changeNickDisplay($aseco, $login) {
        $lastNickUpdate = $this->players[$login]["LastNickUpdate"];
        
        if (time() - $lastNickUpdate >= $this->INTERVAL_NICK_CHANGE) {
            $whiteNick = $this->players[$login]["WhiteNick"];
            $this->players[$login]["WhiteNick"] = !$whiteNick;
            $this->players[$login]["LastNickUpdate"] = time();
            $this->showListToLogin($aseco, $login);
        } else {
            $message = '{#error}You can change your nickname color after ' . $this->INTERVAL_NICK_CHANGE . ' second(s)!';
            $aseco->client->query('ChatSendToLogin', $aseco->formatColors($message), substr_replace($login, "", -1));
        }
    }
    
    public function managePlayerConnect($aseco, $command) {
        $login = $command->login;
        
        $nickname = htmlspecialchars($command->nickname);
        $nickname = $this->removeCapitals($nickname);
        $nicknameWhite = $this->removeColors($nickname);
        
        $aseco->client->query('GetPlayerInfo', $login, 1);
        $info = $aseco->client->getResponse();
        $spec = $info['SpectatorStatus'];
        
        if (is_string($login)) {
            $this->players[$login] = array("Login" => $login,
                "NicknameWhite" => $nicknameWhite,
                "NicknameColored" => $nickname, "Manialink" => true,
                "WhiteNick" => $this->WHITE_NICK, "LastNickUpdate" => 0, "CPNumber" => 0,
                "Time" => '', "Spectator" => $spec);
            
            if (!empty($this->list) && count($this->list) >= $this->MAX_DISPLAY_ROWS) {
                $this->updateList($aseco, $login);
            } else {
                $this->updateList($aseco);
            }
            
            $this->bindToggleKey($aseco, $command);
        }
    }
    
    private function removeCapitals($text) {
        $search = array('$t', '$w', '$o', '$i', '$s', '"', '$z');
        $replace = array("", "", "", "", "", "''", "");
        
        return str_ireplace($search, $replace, $text);
    }
    
    public function managePlayerCheckpoint($aseco, $command) {
        $login = $command[1];
        $this->players[$login]["CPNumber"] = $command[4] + 1;
        $this->players[$login]["Time"] = $command[2];
        
        $this->updateList($aseco);
    }
    
    public function managePlayerFinish($aseco, $command) {
        $login = $command->player->login;
        $cps = $command->challenge->nbchecks;
        
        if ($this->players[$login]["CPNumber"] != $cps) {
            $this->players[$login]["CPNumber"] = 0;
            $this->players[$login]["Time"] = '';
        }
        
        $this->updateList($aseco);
    }
    
    public function checkPlayerAnswer($aseco, $command) {
        $playerLogin = $command[1];
        $login = $playerLogin;
        $answer = $command[2];
        
        if ($answer == $this->ANSWER_TOGGLE_HUD) {
            $maniaLink = $this->players[$login]["Manialink"];
            $this->players[$login]["Manialink"] = !$maniaLink;
            $this->showListToLogin($aseco, $login);
        } elseif ($answer == $this->ANSWER_SWITCH_COLOR) {
            $this->changeNickDisplay($aseco, $login);
        }
    }
    
    public function checkPlayerStatus($aseco, $command) {
        $login = $command["Login"];
        $spec = $command["SpectatorStatus"];
        
        if ($spec) {
            $this->players[$login]["CPNumber"] = 0;
            $this->players[$login]["Time"] = '';
        }
        $this->players[$login]["Spectator"] = $spec;
        
        $this->updateList($aseco);
    }
    
    public function emptyManialinks($aseco) {
        $hud = '<manialinks>';
        $hud .= '<manialink id="' . $this->ID_TITLE_BAR . '" />';
        $hud .= '<manialink id="' . $this->ID_LIST . '" />';
        $hud .= '</manialinks>';
        $aseco->client->query("SendDisplayManialinkPage", $hud, 1, false);
    }
    
    public function reset($aseco) {
        foreach ($aseco->server->players->player_list as $player) {
            $login = $player->login;
            
            $aseco->client->query('GetPlayerInfo', $login, 1);
            $info = $aseco->client->getResponse();
            $spec = $info['SpectatorStatus'];
            
            $this->players[$login]["CPNumber"] = 0;
            $this->players[$login]["Time"] = '';
            $this->players[$login]["Spectator"] = $spec;
        }
        
        $this->getTrackInfo($aseco);
        $this->updateList($aseco);
    }
    
    public function managePlayerDisconnect($aseco, $info) {
        $login = $info->login;
        unset($this->players[$login]);
        
        foreach ($this->list as $player) {
            if ($player["Login"] == $login) {
                $this->updateList($aseco);
                break;
            }
        }
    }
    
    private function getTrackInfo($aseco) {
        $aseco->client->query('GetCurrentChallengeInfo');
        $info = $aseco->client->getResponse();
        $this->numberCPs = ($info['NbCheckpoints']) - 1;
    }
    
    private function compareCpNumbers($a, $b) {
        if ($a["CPNumber"] == $b["CPNumber"]) {
            return 0;
        }
    
        return ($a["CPNumber"] > $b["CPNumber"]) ? -1 : 1;
    }

    private function refillList() {
        $this->list = array();
        
        foreach ($this->players as $player) {
            if (!$player["Spectator"]) {
                $this->list[] = $player;
            }
        }
    
    }
    
    private function updateList($aseco, $login = null) {
        //Liste neu befüllen
        $this->refillList();
        
        if (empty($this->list) == array()) {
            //Liste sortieren
            usort($this->list, array($this, "compareCpNumbers"));
            //Liste begrenzen
            $this->list = array_slice($this->list, 0, $this->MAX_DISPLAY_ROWS);
        }
        
        if ($login == null) {
            $this->showListToAll($aseco);
        } else {
            $this->showListToLogin($aseco, $login);
        }
    }
    
    private function getMilliSeconds() {
        return microtime(true) * 1000;
    }

    public function onEverySecond($aseco) {
        if ($this->needsUpdate) {
            $this->needsUpdate = false;
            $this->showListToAll($aseco);
        }
    }
    
    private function showListToAll($aseco) {
        $time = $this->getMilliSeconds();
        if (($time - $this->lastUpdate) < 400) {
            $this->needsUpdate = true;
            return;
        }
        
        foreach ($this->players as $login => $player) {
            if ($player["Manialink"]) {
                $this->showList($aseco, $login);
            }
        }

        $this->lastUpdate = $this->getMilliSeconds();
    }
    
    private function showListToLogin($aseco, $login) {
        if ($this->players[$login]["Manialink"]) {
            $this->showList($aseco, $login);
        } else {
            $this->hideList($aseco, $login);
        }
    }
    
    private function displayTitleBar($aseco, $login) {
        $hud = '<?xml version="1.0" encoding="UTF-8"?>';
        $hud .= '<manialink id="' . $this->ID_TITLE_BAR . '">';
        $hud .= '<frame scale="1" posn="' . $this->POS_X . ' ' . $this->POS_Y . '">';
        $hud .= '<quad posn="0 0 0" sizen="21 2 0.08" halign="left" valign="center" style="BgsPlayerCard" substyle="BgCard" action="' . $this->ANSWER_SWITCH_COLOR . '"/>';
        $hud .= '<label scale="0.45" posn="0.4 0.1 0.1" halign="left" valign="center" style="TextRaceMessage" text="$' . $this->COLORS['Title'] . ' Checkpoints Live"/>';
        $hud .= '<label scale="0.45" posn="11 0.1 0.1" halign="left" valign="center" style="TextRaceMessage" text="$' . $this->COLORS['TrackText'] . 'Track CPs:"/>';
        $hud .= '<label scale="0.45" posn="17.5 0.1 0.1" halign="center" valign="center" style="TextRaceMessage" text="$' . $this->COLORS['TrackNumber'] . '' . $this->numberCPs . '"/>';
        $hud .= '<quad posn="19 0 0.12" sizen="1.8 1.8" halign="left" valign="center"
                image="http://www.imageshare.web.id/images/08y0yvg4k23h26gbbmg2.png"
                imagefocus="http://www.imageshare.web.id/images/v69z1j9sd9ebkgj9sckf.png" action="' . $this->ANSWER_TOGGLE_HUD . '"/>';
        $hud .= '</frame>';
        $hud .= '</manialink>';
        $aseco->client->query("SendDisplayManialinkPageToLogin", strval($login), $hud, 0, false);
    }
    
    private function showList($aseco, $login) {
        $this->displayTitleBar($aseco, $login);
        
        $hud = '<manialink id="' . $this->ID_LIST . '">';
        
        $y = $this->POS_Y - 1.9;
        $pos = 1;
        
        foreach ($this->list as $player) {
            if ($this->WHITE_NICKS_ALLOWED && $this->players[$login]["WhiteNick"]) {
                $nickname = $player["NicknameWhite"];
            } else {
                $nickname = $player["NicknameColored"];
            }
            
            $hud .= '<frame scale="1" posn="' . $this->POS_X . ' ' . $y . '">';
            
            if ($this->INDICES) {
                $hud .= '<label scale="0.48" posn="0.3 0.1 0.1" halign="left" valign="center" style="TextRaceMessage" text="$' . $this->COLORS['Position'] . '' . str_pad($pos, 2, '0', STR_PAD_LEFT) . '"/>';
            }
            
            if (($player["CPNumber"] - 1) == $this->numberCPs) {
                $hud .= '<label scale="0.48" posn="1.8 0.1 0.1" halign="left" valign="center" style="TextRaceMessage" text="$' . $this->COLORS['CPText'] . 'CP-"/>';
                $hud .= '<quad posn="4 0 0" sizen="1.6 1.6" halign="left" valign="center" style="BgRaceScore2" substyle="Warmup"/>';
            } else {
                $hud .= '<label scale="0.48" posn="1.8 0.1 0.1" halign="left" valign="center" style="TextRaceMessage" text="$' . $this->COLORS['CPText'] . 'CP-$' . $this->COLORS['CPNumber'] . '' . $player["CPNumber"] . '"/>';
            }
            
            $hud .= '<label scale="0.48" posn="10.4 0.1 0.1" halign="right" valign="center" style="TextRaceMessage" text="$' . $this->COLORS['Time'] . '' . formatTime(intval($player["Time"])) . '"/>';
            $hud .= '<label scale="0.48" posn="10.6 0.1 0.1" sizen="21 2" halign="left" valign="center" style="TextRaceMessage" text="' . $nickname . '"/>';
            $hud .= '</frame>';
            
            $y -= 1.8;
            $pos++;
        }
        
        if ($this->BACKGROUND) {
            $rows = count($this->list);
            $qheight = ($rows * 1.8) + 0.6;
            $hud .= '<quad posn="' . $this->POS_X . ' ' . ($this->POS_Y - 0.7) . '" sizen="21 ' . $qheight . ' 0.08" halign="left" valign="top" style="Bgs1InRace" substyle="NavButton"/>';
        }
        
        $hud .= '</manialink>';
        $aseco->client->query("SendDisplayManialinkPageToLogin", strval($login), $hud, 0, false);
    }
    
    private function removeColors($nickname) {
        return preg_replace('/\$[0-9A-F]{3}/i', '', $nickname);
    }
    
    private function hideList($aseco, $login) {
        $hud = '</manialinks>';
        $hud .= '<manialink id="' . $this->ID_TITLE_BAR . '">';
        $hud .= '<frame scale="1" posn="' . $this->POS_X . ' ' . $this->POS_Y . '">';
        $hud .= '<quad posn="0 0 0" sizen="6.8 2 0.08" halign="left" valign="center" style="BgsPlayerCard" substyle="BgCard" />';
        $hud .= '<label scale="0.45" posn="0.4 0.1 0.1" halign="left" valign="center" style="TextRaceMessage" text="$' . $this->COLORS['Title'] . ' CP Live"/>';
        $hud .= '<quad posn="4.9 0 0.12" sizen="1.8 1.8 " halign="left" valign="center" image="http://www.imageshare.web.id/images/q7xwtmnuxbrble2eow3h.png" imagefocus="http://www.imageshare.web.id/images/hb2te8udrnlm03yo6yy2.png" action="' . $this->ANSWER_TOGGLE_HUD . '"/>';
        $hud .= '</frame>';
        $hud .= '</manialink>';
        $hud .= '<manialink id="' . $this->ID_LIST . '" />';
        $hud .= '</manialinks>';
        $aseco->client->query("SendDisplayManialinkPageToLogin", strval($login), $hud, 0, false);
    }
}
function cpLiveInit() {
    global $cpLive, $aseco;
    $cpLive = new CPLiveAdvanced();
    $aseco->registerEvent('onSync', array($cpLive, 'onSync'));
    $aseco->registerEvent('onRestartChallenge', array($cpLive, 'emptyManialinks'));
    $aseco->registerEvent('onEndRound', array($cpLive, 'emptyManialinks'));
    $aseco->registerEvent('onPlayerConnect', array($cpLive, 'managePlayerConnect'));
    $aseco->registerEvent('onCheckpoint', array($cpLive, 'managePlayerCheckpoint'));
    $aseco->registerEvent('onPlayerFinish', array($cpLive, 'managePlayerFinish'));
    $aseco->registerEvent('onPlayerManialinkPageAnswer', array($cpLive, 'checkPlayerAnswer'));
    $aseco->registerEvent('onPlayerInfoChanged', array($cpLive, 'checkPlayerStatus'));
    $aseco->registerEvent('onBeginRound', array($cpLive, 'reset'));
    $aseco->registerEvent('onPlayerDisconnect', array($cpLive, 'managePlayerDisconnect'));
    $aseco->registerEvent('onEverySecond', array($cpLive, 'onEverySecond'));
    $aseco->addChatCommand(
        'cpLive',
        'CPLive chat commands: help, color, toggle(' . $cpLive->KEYS[$cpLive->TOGGLE_KEY] . ')'
    );
}
cpLiveInit();

function chat_cpLive($aseco, $command) {
    global $cpLive;
    $cpLive->manageChatCommands($aseco, $command);
}
